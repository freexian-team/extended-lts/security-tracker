# Renamed/branched/forked packages, probably affected by the same CVEs
#   Meant for ELTS-specific, regular, automated CVE association
# Format: elts-package... < lts-package...
# Process with bin/related-cves.py.

# buster < bullseye [and later]
boost1.67 < boost1.74
golang-1.11 < golang-1.15 golang-1.19
guile-2.0 < guile-2.2
gtksourceview2 < gtksourceview3
libcoap < libcoap2
mailman < mailman3
mariadb-10.3 < mariadb-10.5
php7.3 < php7.4
postgresql-11 < postgresql-13
python3.7 < python3.9
# python2 ecosystem
python2.7 < python3.9 python3.11
pypy      < python2.7 pypy3
jython    < python2.7
ruby2.5 < ruby2.7
slurm-llnl < slurm-wlm
sqlite < sqlite3
twig < php-twig

# stretch < buster
boost1.62 < boost1.67
botan1.10 < botan
cfengine2 < cfengine3
emacs25 < emacs
freerdp < freerdp2
golang-1.7 golang-1.8 < golang-1.11
libical < libical3
libidn2-0 < libidn
lucene2 < lucene4.10
mariadb-10.1 < mariadb-10.3
nagios3 < nagios4
netty-3.9 < netty
openssl1.0 < openssl
php7.0 < php7.3
pike7.8 < pike8.0
postgresql-9.6 < postgresql-11
python3.5 < python3.7
ruby2.3 < ruby2.5
squid3 < squid
tcl8.5 < tcl8.6
tk8.5 < tk8.6
tomcat8 < tomcat9
unbound1.9 < unbound
# special case: gnupg1 untracked by secteam in buster-lts already
gnupg1 < gnupg2

# jessie < stretch
boost1.55 < boost1.62
clojure1.2 clojure1.4 clojure1.6 < clojure
emacs24 < emacs25
firebird2.5 < firebird3.0
gnupg < gnupg1
golang < golang-1.7 golang-1.8
gst-plugins-bad0.10 < gst-plugins-bad1.0
gst-plugins-base0.10 < gst-plugins-base1.0
gst-plugins-good0.10 < gst-plugins-good1.0
gst-plugins-ugly0.10 < gst-plugins-ugly1.0
gstreamer0.10 < gstreamer1.0
guile-1.8 < guile-2.0
jetty jetty8 < jetty9
libmusicbrainz3 < libmusicbrainz5
lucene4 < lucene4.10
lynx-cur < lynx
mariadb-10.0 < mariadb-10.1
ogre-1.8 < ogre-1.9
openjdk-7 < openjdk-8
php5 < php7.0
postgresql-9.1 postgresql-9.4 < postgresql-9.6
python3.4 < python3.5
ruby2.1 < ruby2.3
tomcat6 tomcat7 < tomcat8
transfig < fig2dev

# OpenJDK is in unstable as of 2024-08 and still being triaged, don't interfere
#openjdk-8 < openjdk-11
