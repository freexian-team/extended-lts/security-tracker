#!/usr/bin/python3
# Report or add CVE entries for related packages
# Copyright 2021, 2023, 2024  Sylvain Beucler
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

# Meant for inclusion in the Debian security-tracker:
# https://salsa.debian.org/lts-team/lts-extra-tasks/-/issues/12

import setup_paths

import sys, os
import functools, operator
from sectracker import parsers
import bugs, debian_support
import argparse

class Packages2CVEs:
    """
    Import multiple sources of package/CVE relations, for efficient queries
    """
    def __init__(self, cvelist_main_filename,
                 cvelist_ext_filename,
                 dtsalist_filename, dsalist_filename, dlalist_filename,
                 extadvlist_filename):

        self.packages2cves = {}
        self.bugs = {os.path.realpath(cvelist_main_filename):{}}
        if cvelist_ext_filename:
            self.bugs[os.path.realpath(cvelist_ext_filename)] = {}

        self.cvelist_main = parsers.cvelist(cvelist_main_filename)
        self.import_cvelist(self.cvelist_main)
        if cvelist_ext_filename is not None:
            self.cvelist_ext = parsers.cvelist(cvelist_ext_filename)
            self.import_cvelist(self.cvelist_ext)

        adv_dtsa = parsers.dtsalist(dtsalist_filename)
        adv_dsa = parsers.dsalist(dsalist_filename)
        adv_dla = parsers.dlalist(dlalist_filename)
        self.import_advisories(adv_dtsa)
        self.import_advisories(adv_dsa)
        self.import_advisories(adv_dla)
        if extadvlist_filename is not None:
            adv_ext = parsers.extadvlist(extadvlist_filename)
            self.import_advisories(adv_ext)

    @staticmethod
    def bug_id(bug):
        """
        Convert CVE-date-XXXX->TEMP-btsbug-checksum.
        Makes each bug entry unique and allows cross-referencing in cvelist_ext.
        """
        cve_id = bug.header.name
        if cve_id.split('-')[2] == 'XXXX':
            btsbug = 0
            for annotation in bug.annotations:
                if annotation.type != 'package':
                    continue
                for flag in annotation.flags:
                    if isinstance(flag, parsers.PackageBugAnnotation):
                        btsbug = flag.bug
                        break  # first btsbug id
                if btsbug > 0:
                    break
            cve_id = bugs.temp_bug_name(btsbug, bug.header.description[1:-1])
        return cve_id

    def import_cvelist(self, cvelist):
        """
        For each package, which CVEs affect it
        """
        for bug in cvelist:
            bug_id = self.bug_id(bug)
            for annotation in bug.annotations:
                if annotation.type != 'package':
                    continue
                if annotation.release is not None:
                    continue
                self.packages2cves.setdefault(annotation.package,set()) \
                                  .add(bug_id)
            self.bugs[os.path.realpath(bug.file)][bug_id] = bug

    def import_advisories(self, advlist):
        for adv in advlist:
            bugs = set()
            package = None
            for annotation in adv.annotations:
                if annotation.type == 'xref':
                    bugs = set(annotation.bugs)
                if annotation.type == 'package':
                    package = annotation.package
            self.packages2cves.setdefault(package,set()).update(bugs)

    def get_cves_for_packages(self, *packages):
        """
        Return the list of CVEs that affect the given packages
        """
        matching_cves = set()
        for package in packages:
            matching_cves.update(self.packages2cves.get(package,set()))
        return matching_cves


class PackageOneWayMatcher:
    """
    Read package transitions, e.g. {oldname: [newname1, newname2]}.
    Supports merging multiple files.
    renamed-packages format: oldpackage... < newpackage...
    Also supports data/embedded-code-copies for experimenting
    """
    def __init__(self, format='renamed-packages'):
        self.related_packages = {}
        self.format = format

    def load(self, *filenames):
        if self.format == 'renamed-packages':
            self.load_renames(*filenames)
        elif self.format == 'embedded-code-copies':
            self.load_embedded_code_copies(*filenames)
        else:
            raise Exception("Unknown format %s" % self.format)

    def load_renames(self, *filenames):
        for filename in filenames:
            if filename is None:
                continue
            with open(filename) as f:
                lineno = 0
                for line in f:
                    lineno += 1
                    line = line.rstrip('\n')
                    if len(line) == 0 or line.startswith('#'):
                        continue
                    packages = line.split()
                    if not '<' in packages:
                        raise Exception("Missing '<' in %s line %d" % (filename, lineno))
                    mid = packages.index('<')
                    dst = packages[:mid]
                    src = packages[mid+1:]
                    for p in dst:
                        self.related_packages.setdefault(p, set()).update(src)

    def load_embedded_code_copies(self, *filenames):
        for filename in filenames:
            if filename is None:
                continue
            with open(filename) as f:
                lineno = 0
                while True:
                    line = f.readline()
                    if line == '':
                        break
                    if line.startswith('---BEGIN'):
                        break
                while True:
                    line = f.readline()
                    if line == '':
                        break
                    package = line.split()[0]
                    while True:
                        line = f.readline()
                        if line in ['\n', '']:
                            break
                        line = line.lstrip()
                        if line.startswith('-'):
                            self.related_packages.setdefault(line.split()[1], set()).add(package)

    def get_related_to(self, package):
        return self.related_packages.get(package, set())

    def get_packages(self):
        return self.related_packages.keys()

class PackageTwoWayMatcher:
    """
    Read non-hierarchical, related package sets, e.g. (mypackage1.2, mypackage1.4, mypackage1.7).
    Supports merging multiple files.
    """
    def __init__(self):
        self.package_groups = []

    def load(self, *filenames):
        for filename in filenames:
            if filename is None:
                continue
            with open(filename) as f:
                for line in f:
                    line = line.rstrip('\n')
                    if len(line) == 0 or line.startswith('#'):
                        continue
                    packages = line.split()
                    present = False
                    for package in packages:
                        if package == '<':
                            continue
                        for existing_set in self.package_groups:
                            if package in existing_set:
                                existing_set.update(packages)
                                present = True
                    if not present:
                        self.package_groups.append(set(packages))

    def get_related_to(self, package):
        for group in self.package_groups:
            if package in group:
                return group
        return set()

    def get_packages(self):
        return functools.reduce(operator.or_, self.package_groups)



def refresh_source_packages_list():
    # Refresh reference list of source packages in sid
    # Cf. Makefile:update-$(1), but just what we need, for speed
    for section in ['main', 'contrib', 'non-free']:
        os.system('bin/apt-update-file http://ftp.debian.org/debian/dists/sid/%s/source/Sources data/packages/sid__%s_Sources'
                  % (section, section))

def print_cves(cves, sid_packages):
    for cve in cves:
        print("%s" % cve)
        if package in sid_packages:
            print("\t- %s <unfixed>" % package)
        else:
            print("\t- %s <removed>" % package)

def updated_cves(cves, sid_packages, cvelist_save_filename, cvelist_save, p2c):
    """
    Update and return cvelist_save with modified or added entries, suitable for saving
    """
    cvelist_save_filename = os.path.realpath(cvelist_save_filename)
    for cve in missing_cves:
        if package in sid_packages:
            kind = 'unfixed'
        else:
            kind = 'removed'
        annotation = parsers.PackageAnnotation(line=0, type='package', release=None, package=package, kind=kind, version=None, description=None, flags=[])
        if cve in p2c.bugs[cvelist_save_filename]:  # query using unique id (i.e. handle CVE-XXXX/TEMP entries)
            p2c.bugs[cvelist_save_filename][cve].annotations.append(annotation)
        else:
            bug = parsers.Bug(args.extcve,
                              parsers.Header(line=0, name=cve, description=''),
                              [annotation])
            cvelist_save.append(bug)
    return cvelist_save


parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='Investigate untracked CVEs from related packages. Report or track them.',
    epilog='''Examples:

# Report CVE entries that may have been missed for old renamed packages in Debian
$ bin/related-cves.py --transitions data/packages/renamed-packages --report

# Also report CVE entries that may have been missed for newly branched packages in Debian (e.g. the golang-1.xx set)
$ bin/related-cves.py --transitions data/packages/renamed-packages --report --two-way --start-year 2021

# Automatically add entries for renamed packages in ELTS, in the extended CVE list file, restricting to supported packages
$ bin/related-cves.py --transitions data/packages/renamed-packages.elts --extcve data/CVE-EXTENDED-LTS/list --extadv data/ELA/list \\
    --start-year 2023 \\
    $(cat ../extended-lts/packages-to-support-buster) && \\
  bin/related-cves.py --transitions data/packages/renamed-packages.elts --extcve data/CVE-EXTENDED-LTS/list --extadv data/ELA/list \\
    --start-year 2021 \\
    $(cat ../extended-lts/packages-to-support-stretch) && \\
  bin/related-cves.py --transitions data/packages/renamed-packages.elts --extcve data/CVE-EXTENDED-LTS/list --extadv data/ELA/list \\
    --start-year 2019 \\
    $(cat ../extended-lts/packages-to-support-jessie)

# Check issues that might affect krfb and vino in Debian (due to embedding e.g. libvncserver)
$ bin/related-cves.py --transitions data/embedded-code-copies --format embedded-code-copies --report krfb vino

# Check issues that might affect php5 in ELTS (due to embedding various libraries)
$ bin/related-cves.py --transitions data/embedded-code-copies --extadv data/ELA/list --format embedded-code-copies --report php5
'''
)
parser.add_argument('--report', action='store_true', help='output related CVEs, grouped by package (default: modify CVE list file)')
parser.add_argument('--transitions', help='package transitions file (default: data/packages/renamed-packages)', default=os.path.dirname(__file__)+'/../data/packages/'+'renamed-packages')
parser.add_argument('--format', help='transitions file format (renamed-packages or embedded-code-copies)', default='renamed-packages')
parser.add_argument('--two-way', action='store_true', help='add missing CVEs to both sides of package transitions (default: one-way)')
parser.add_argument('--extcve', help='extended CVE file to load, also becomes changes target (e.g. data/CVE-XXXX/list)')
parser.add_argument('--extadv', help='extended advisory file (e.g. data/XXX/list)')
parser.add_argument('--exttransitions', help='additional package transitions file (default: none)')
parser.add_argument('--start-year', default=0, type=int, help='ignore CVEs prior to that date, e.g. the start of an extended support release (heuristic: based on CVE-YEAR-xxx) (default: %(default)d)')
parser.add_argument('packages', nargs='*', metavar='PACKAGE', help='restrict action to these packages (default: all)')
args = parser.parse_args()


p2c = Packages2CVEs(os.path.dirname(__file__)+'/../data/CVE/list',
                    args.extcve,
                    os.path.dirname(__file__)+'/../data/DTSA/list',
                    os.path.dirname(__file__)+'/../data/DSA/list',
                    os.path.dirname(__file__)+'/../data/DLA/list',
                    args.extadv,
)

refresh_source_packages_list()
sid_packages = set(parsers.sourcepackages(os.path.dirname(__file__)+'/../data/packages/sid__main_Sources').keys())

if args.format not in ['renamed-packages', 'embedded-code-copies']:
    print("Unsupported transitions format.")
    sys.exit(1)
if args.format == 'renamed-packages':
    if args.two_way:
        pm = PackageTwoWayMatcher()
    else:
        pm = PackageOneWayMatcher()
else:
    pm = PackageOneWayMatcher(format='embedded-code-copies')
pm.load(args.transitions, args.exttransitions)

target_packages = args.packages
if len(target_packages) == 0:
    target_packages = pm.get_packages()

cvelist_save_filename = os.path.dirname(__file__)+'/../data/CVE/list'
cvelist_save = p2c.cvelist_main
if args.extcve:
    cvelist_save_filename = args.extcve
    cvelist_save = p2c.cvelist_ext
sort_key = functools.cmp_to_key(debian_support.version_compare)

for package in target_packages:
   # relationships
   related_packages = pm.get_related_to(package)
   package_cves = p2c.get_cves_for_packages(package)
   related_cves = p2c.get_cves_for_packages(*related_packages)
   missing_cves = related_cves.difference(package_cves)

   # additional filters
   # - year
   missing_cves = [c for c in missing_cves
                   if not(c.startswith('CVE') and int(c.split('-')[1]) < args.start_year)]

   missing_cves = sorted(missing_cves, key=sort_key)
   if args.report:
       print_cves(missing_cves, sid_packages)
   else:
       cvelist_save = updated_cves(missing_cves, sid_packages, cvelist_save_filename, cvelist_save, p2c)


if not args.report:
    with open(cvelist_save_filename, 'w') as f:
        parsers.writecvelist(cvelist_save, f)
