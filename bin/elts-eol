#!/bin/sh
# Requires:
# wget, html2text
set -e

RELEASES="jessie stretch buster"
FILE="data/CVE-EXTENDED-LTS/list"
WDB="https://deb.freexian.com/extended-lts/tracker/status/release"
PTS="../extended-lts/packages-to-support"
ENF="data/ela-needed.txt"
OPF=$(mktemp)

for RELEASE in $RELEASES; do
    RELPAGE="$WDB/$RELEASE"
    RELPTS="$PTS-$RELEASE"
    [ ! -f "$RELPTS" ] && echo "couldn't find $RELPTS" && exit 1
    echo "Fetching tracker db from $RELPAGE"
    wget -O "$OPF" -q "$RELPAGE"
    html2text "$OPF" | grep -E "(CVE|TEMP)-" | while read A B O ; do
        if echo $A | grep -q -E "^(CVE|TEMP)-" ; then
            CVE=$A
        else
            PACKAGE=$A
            CVE=$B
        fi

        # Sanitize package name for e.g. foo_(non-free)
        PACKAGE=${PACKAGE%%_*}

        if echo "$PACKAGE" | grep -q '^[-a-zA-Z0-9_][-a-zA-Z0-9_.+]*$' ; then
            if [ -n "$CVE" ] ; then
                if grep -q "^$PACKAGE$" $RELPTS ; then
                    if grep -P -q "^$PACKAGE(/([a-z]+,)*$RELEASE(,[a-z]+)*)? \(" $ENF ; then
                        echo "Supported $PACKAGE/$RELEASE $CVE (Claimed)"
                    elif grep -P -q "^$PACKAGE(/([a-z]+,)*$RELEASE(,[a-z]+)*)?$" $ENF ; then
                        echo "Supported $PACKAGE/$RELEASE $CVE (Not claimed)"
                    else
                        echo "Supported $PACKAGE/$RELEASE $CVE (Triage needed)"
                    fi
                else
                    if grep -q "^$CVE$" $FILE ; then
                        if grep -A2 "^$CVE$" $FILE | grep -q "\\[$RELEASE\\] - $PACKAGE <end-of-life>" ; then
                            echo "Entry already exist for $PACKAGE in $CVE."
                        else
                            echo "Entry already exist for $CVE, updating it"
                            sed -i "s/^$CVE$/\0\n\t[$RELEASE] - $PACKAGE <end-of-life>/" $FILE
                        fi
                    else
                        echo "EOL $PACKAGE $CVE adding entry."
                        echo "$CVE" >> $FILE
                        echo "\t[$RELEASE] - $PACKAGE <end-of-life>" >> $FILE
                    fi
                fi
            else
                echo "Empty CVE for $PACKAGE"
            fi
        else
            echo "Unknown characters in package name $PACKAGE."
        fi
    done
    rm -f "$OPF"
done

./bin/elts-sort-cve-file

if [ "$1" != "--no-cleanup" ]; then
    # drop extra <end-of-life> we added before CVEs were marked unimportant
    bin/elts-trim-unimportant
fi
