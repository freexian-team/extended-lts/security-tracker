define get_config =
$(shell jq -r $(1) 'data/config.json')
endef

ELTS_RELEASES = jessie stretch buster
ELTS_MIRROR = http://deb.freexian.com/extended-lts

RELEASES := $(filter-out $(foreach release,$(ELTS_RELEASES),$(release) $(release)_security),$(RELEASES))

define add_main_release =
$(1)_MIRROR = $$(ELTS_MIRROR)
$(1)_DIST = $(1)
$(1)_ARCHS = $(call get_config, '.distributions.$(1).architectures[]')
$(1)_RELEASE = $(1)
$(1)_SUBRELEASE =
RELEASES += $(1)
endef

define add_lts_release =
$(1)_lts_MIRROR = $$(ELTS_MIRROR)
$(1)_lts_DIST = $(1)-lts
$(1)_lts_ARCHS = $$($(1)_ARCHS)
$(1)_lts_RELEASE = $(1)
$(1)_lts_SUBRELEASE = lts
RELEASES += $(1)_lts
endef

$(foreach release,$(ELTS_RELEASES),$(eval $(call add_main_release,$(release))))
$(foreach release,$(ELTS_RELEASES),$(eval $(call add_lts_release,$(release))))

RELEASES := $(filter-out %_backports,$(RELEASES))
